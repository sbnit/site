<?php
namespace App\Http\Requests\Location;

use Illuminate\Foundation\Http\FormRequest;

class GetLocationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'search' => 'string|nullable|max:255',
        ];
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
