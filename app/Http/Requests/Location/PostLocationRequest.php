<?php
namespace App\Http\Requests\Location;

use Illuminate\Foundation\Http\FormRequest;

class PostLocationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'location_id' => 'integer|required',
            'language_id' => 'integer|required',
        ];
    }

    public function messages(): array
    {
        return [
            'location_id.required' => __('locale.noSelectCountry'),
            'language_id.required' => __('locale.noSelectLanguage'),
            'location_id.integer' => __('locale.noSelectCountry'),
            'language_id.integer' => __('locale.noSelectLanguage'),
        ];
    }
}
