<?php
namespace App\Http\Requests\Languages;

use Illuminate\Foundation\Http\FormRequest;

class GetLanguagesRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'search' => 'string|nullable|max:2'
        ];
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
