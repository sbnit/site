<?php
namespace App\Http\Controllers\Views;

use App\Models\Clients\Setting;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Contracts\View\View;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected string $title = '';

    protected string $keywords = '';
    protected string $description = '';
    protected string $author = 'SBNIT';

    protected array $css = [];
    protected array $js = [];

    abstract protected function index(): string;

    abstract protected function data(): array;

    protected function getData(): array
    {
        $data = [
            'phone' => Setting::phone(),
            'email' => Setting::email()
        ];

        return array_merge($data, $this->data());
    }

    protected function view(): View
    {
        return view('pages.'.$this->index())
            ->with('title', $this->title)
            ->with('keywords', $this->keywords)
            ->with('description', $this->description)
            ->with('author', $this->author)
            ->with('css', $this->css)
            ->with('js', $this->js)
            ->with('data', $this->getData());
    }
}
