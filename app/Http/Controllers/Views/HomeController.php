<?php
namespace App\Http\Controllers\Views;

use App\Models\Clients\Survey;
use JetBrains\PhpStorm\NoReturn;

class HomeController extends Controller
{

    #[NoReturn] public function __construct(){
        $this->title = config('app.name').' - '.__('settings.home');

        $this->keywords = '';
        $this->description = '';

        $this->css = [];
        $this->js = [];
    }

    protected function index (): string
    {
        return 'home';
    }

    /**
     * @return array
     */
    protected function data (): array
    {
        return [
            'survey' => Survey::getRandomString(),
        ];
    }
}
