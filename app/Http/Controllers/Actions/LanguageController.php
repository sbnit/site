<?php

namespace App\Http\Controllers\Actions;

use App\Http\Requests\Languages\GetLanguagesRequest;
use App\Models\Language as Model;
use Illuminate\Http\JsonResponse;

class LanguageController extends Controller
{
    protected function model(): Model
    {
        return new Model();
    }

    protected function service()
    {
        return null;
    }

    protected function get(GetLanguagesRequest $request): JsonResponse
    {
        $search = $request['search'];

        $result = $this->model()->search($search);
        $resultArray = $result ? $result->toArray() : [];

        return response()->json($resultArray);
    }
}
