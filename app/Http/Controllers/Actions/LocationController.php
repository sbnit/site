<?php
namespace App\Http\Controllers\Actions;

use App\Http\Requests\Location\GetLocationRequest;
use App\Http\Requests\Location\PostLocationRequest;
use App\Models\Language;
use App\Models\Location as Model;
use Illuminate\Http\JsonResponse;

class LocationController extends Controller
{
    protected function model(): Model
    {
        return new Model();
    }

    protected function service()
    {
        return null;
    }


    protected function get(GetLocationRequest $request): JsonResponse
    {
        $search = $request['search'];
        $result = $this->model()->search($search);
        $resultArray = $result ? $result->toArray() : [];

        return response()->json($resultArray);
    }

    protected function post(PostLocationRequest $request): JsonResponse
    {
        $location_id = (int)$request['location_id'];
        $language_id = (int)$request['language_id'];

        $LocationResult = $this->model()->findOrFail($location_id);
        $LanguageResult = Language::findOrFail($language_id);

        session(['location' => $LocationResult->name]);
        session(['language' => [
            'name' => $LanguageResult->name,
            'shortname' => $LanguageResult->shortname
        ]]);
        session(['connection' => $LocationResult->client->name]);

        return response()->json(['status' => true]);
    }
}
