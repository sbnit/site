<?php
namespace App\Http\Middleware;

use App\Facades\Repositories\Location as Repository;
use App\Facades\Client;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class Location
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle(Request $request, Closure $next)
    {
//        session(['location' => null]);
//        session(['language' => null]);
//        session(['connection' => null]);


        $language = session('language') ? session('language')['shortname'] : Config::get('app.default_lang');
        $location = session('location') ?: Config::get('app.default_locale');

        $client = Repository::getByName($location)->client;

        $clientId = $client->id ??
            throw new Exception('Client is not found');

        Client::setConnection($clientId);

        app()->setLocale($language);

        return $next($request);
    }
}
