<?php
namespace App\Models;

use App\Abstracts\ModelAbstract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property string $name
 * @property string $host
 * @property string $username
 * @property string $password
 * @property string $database
 */

class Client extends ModelAbstract
{
    use HasFactory;

    public function location(): HasOne
    {
        return $this->hasOne(Location::class);
    }
}
