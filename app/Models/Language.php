<?php
namespace App\Models;

use App\Abstracts\ModelAbstract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property string $name
 * @property string $shortname
 */

class Language extends ModelAbstract
{
    use HasFactory;

    public function search($search): array|Collection
    {
        return self::where('name', 'ilike', "%$search%")->get();
    }
}
