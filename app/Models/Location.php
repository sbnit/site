<?php
namespace App\Models;

use App\Abstracts\ModelAbstract;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property string $name
 * @property string $client_id
 */

class Location extends ModelAbstract
{
    use HasFactory;

    public function client(): HasOne
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public static function search($search): array|Collection
    {
        return self::where('name', 'ilike', "%$search%")->get();
    }

    public static function getByName($name){
        return self::where('name', 'ilike', "%$name%")->get();
    }
}
