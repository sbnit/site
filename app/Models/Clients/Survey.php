<?php
namespace App\Models\Clients;

use App\Abstracts\Clients\ModelAbstract;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property string $text
 */

class Survey extends ModelAbstract
{
    use HasFactory;

    public static function getRandomString(){
        return self::inRandomOrder()->first()->text;
    }
}
