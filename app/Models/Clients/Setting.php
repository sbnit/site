<?php
namespace App\Models\Clients;

use App\Abstracts\Clients\ModelAbstract;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Setting extends ModelAbstract
{
    use HasFactory;

    public static function phone(){
        return self::where('key', 'phone')->first()->value;
    }

    public static function email(){
        return self::where('key', 'email')->first()->value;
    }
}
