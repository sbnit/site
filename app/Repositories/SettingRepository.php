<?php
namespace App\Repositories;

use App\Models\Clients\Setting as Model;

class SettingRepository extends Repository
{

    protected function loadModel(): Model
    {
        return new Model();
    }

    public function phone(): string
    {
        return $this->model->where('key', 'phone')->first()->value;
    }

    public function email(){
        return $this->model->where('key', 'email')->first()->value;
    }
}
