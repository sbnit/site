<?php
namespace App\Repositories;

use App\Models\Location as Model;

class LocationRepository extends Repository
{

    protected function loadModel(): Model
    {
        return new Model();
    }

    public function getByName(string $name): Model|null
    {
        return $this->model->where('name', $name)->first();
    }
}
