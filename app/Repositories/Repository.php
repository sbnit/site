<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class Repository
{
    protected Model $model;

    public function __construct(){
        $this->model = $this->loadModel();
    }

    /**
     * @return Model
     */
    abstract protected function loadModel(): Model;

    /**
     * @param int $id
     * @return Model
     */
    protected function get(int $id): Model
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @return Model
     */
    protected function getAll(): Model
    {
        return $this->model::all();
    }

    /**
     * @param array $attributes
     * @return Model
     */
    protected function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    /**
     * @param array $attributes
     * @return Model|bool
     */
    protected function update(array $attributes): Model|bool
    {
        return $this->model->update($attributes);
    }

    /**
     * @param int $id
     * @return Model
     */
    protected function delete(int $id): Model
    {
        return $this->model->findOrFail($id)->delete($id);
    }
}
