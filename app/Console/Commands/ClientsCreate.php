<?php

namespace App\Console\Commands;

use App\Models\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClientsCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clients:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create clients databases';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $Clients = Client::all();
        $user = config('database.user');

        foreach ($Clients as $client){
            if(!DB::select("SELECT datname FROM pg_database WHERE datname='{$client->database}';")){
                DB::statement("CREATE DATABASE {$client->database};");
                DB::statement("REVOKE CONNECT ON DATABASE {$client->database} FROM PUBLIC;");

                $this->comment("<info>Database:</info> {$client->database} <info>created</info>");
            }else{
                $this->comment("<info>Database:</info> {$client->database} <warn>is exists</warn>");
            }

            if(!DB::select("SELECT usename AS role_name, CASE WHEN usesuper AND usecreatedb THEN CAST('superuser, create database' AS pg_catalog.text) WHEN usesuper THEN CAST('superuser' AS pg_catalog.text) WHEN usecreatedb THEN CAST('create database' AS pg_catalog.text) ELSE CAST('' AS pg_catalog.text) END role_attributes FROM pg_catalog.pg_user WHERE usename='$client->username' ORDER BY role_name desc;")) {
                DB::statement("CREATE USER {$client->username} WITH ENCRYPTED PASSWORD '{$client->password}';");

                $this->comment("<info>User:</info> {$client->username} <success>created</success>");
            }else{
                $this->comment("<info>User:</info> {$client->username} <warn>is exists</warn>");
            }

            DB::statement("GRANT CONNECT ON DATABASE {$client->database} TO {$client->username};");
            DB::statement("GRANT CONNECT ON DATABASE {$client->database} TO {$user};");
            $this->comment("<info>GRANT CONNECT:</info> {$client->database} TO {$client->username}");
            $this->comment("<info>GRANT CONNECT:</info> {$client->database} TO {$user}");
        }

        return 0;
    }
}
