<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class LaravelSetting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel:setting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for setting project';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        Artisan::call('key:generate');
        $this->info('Key generate complete');

        Artisan::call('migrate');
        $this->info('Migration complete');

        Artisan::call('db:seed --class=ClientSeeder');
        $this->info('Seed Client complete');

        Artisan::call('clients:create');
        $this->info('Client Create complete');

        Artisan::call('migrate --path=database/migrations/clients');
        $this->info('Migrate clients complete');

        Artisan::call('db:seed');
        $this->info('Seed complete');

        Artisan::call('optimize');
        $this->info('Optimize complete');

        return 0;
    }
}
