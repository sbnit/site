<?php
namespace App\Services;

use App\Models\Client;
use Config;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ClientService
{
    /**
     * @var Client|Collection
     */
    private Client|Collection $client;

    /**
     * SlaveConnection constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function setConnection(int $id): void
    {
        $this->client = $this->client::findOrFail($id);

        if(!$this->client){
            throw new Exception("Id client: $id is not found");
        }

        Config::set('database.connections.' . $this->client->name, [
            'driver'   => 'pgsql',
            'host'     => $this->client->host,
            'port'     => 5432,
            'database' => $this->client->database,
            'username' => $this->client->username,
            'password' => $this->client->password,
        ]);

        $this->checkConnection($this->client->name);
    }

    /**
     * @return string|null
     */
    public function getConnection(): string|null
    {
        return $this->client->name;
    }

    /**
     * @throws Exception
     */
    public function checkConnection(string $connection): void
    {
        try {
            DB::connection($connection)->getPdo();
        } catch (Exception $e) {
            throw new Exception('Could not connect to the database. Please call command: slave:create. Error: ' . $e->getMessage());
        }
    }
}
