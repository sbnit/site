<?php
namespace App\Facades;

use App\Services\ClientService;
use Illuminate\Support\Facades\Facade;

/**
 * @method static void setConnection(int $id)
 * @method static string getConnection()
 *
 * @see \App\Services\ClientService
 */
class Client extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return ClientService::class;
    }
}
