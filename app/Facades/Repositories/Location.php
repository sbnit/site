<?php
namespace App\Facades\Repositories;

use App\Repositories\LocationRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Facade;

/**
 * @method static Model|null getByName(string $name)
 *
 * @see \App\Repositories\LocationRepository
 */

class Location extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return LocationRepository::class;
    }
}
