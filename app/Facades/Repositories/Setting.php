<?php

namespace App\Facades\Repositories;

use App\Repositories\SettingRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Facade;

/**
 * @method static Model|null phone()
 * @method static Model|null email()
 *
 * @see \App\Repositories\LocationRepository
 */

class Setting extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return SettingRepository::class;
    }
}
