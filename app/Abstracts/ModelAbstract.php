<?php
namespace App\Abstracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 *
 * @method static Model find(int $id)
 * @method static Model findOrFail(int $id)
 * @method static Model firstOrCreate(array $attributes)
 * @method static Model where(string $key, string $value)
 * @method static Model|Collection create(array $attributes)
 *
 * @method static Collection first(string $limit = '')
 */

abstract class ModelAbstract extends Model
{

}
