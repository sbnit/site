<?php
namespace App\Abstracts\Clients;

use App\Facades\Client;
use App\Models\Client as ClientModel;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Migrations\Migration;

abstract class MigrationAbstract extends Migration
{
    /**
     * @var ClientModel|Collection
     */
    protected ClientModel|Collection $clients;

    abstract public function upCustom(string $connection): void;
    abstract public function downCustom(string $connection): void;

    /**
     * @throws Exception
     */
    public function __construct(){
        $this->clients = ClientModel::all();

        if(!$this->clients){
            throw new Exception('Before migrating tables to client databases, first seeding the client table');
        }
    }

    /**
     * @throws Exception
     */
    public function up(): void
    {
        $this->invoke('up');
    }

    /**
     * @throws Exception
     */
    public function down(): void
    {
        $this->invoke('down');
    }

    /**
     * @throws Exception
     */
    private function invoke(string $call): void
    {
        foreach ($this->clients as $client) {
            Client::setConnection($client->id);

            switch($call){
                case 'up':
                    $this->upCustom(Client::getConnection());
                    break;
                case 'down':
                    $this->downCustom(Client::getConnection());
                    break;
            }
        }
    }
}
