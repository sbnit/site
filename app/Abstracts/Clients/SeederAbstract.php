<?php
namespace App\Abstracts\Clients;

use App\Facades\Client;
use App\Models\Client as ClientModel;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

abstract class SeederAbstract extends Seeder
{
    /**
     * @var ClientModel|Collection
     */
    private ClientModel|Collection $clients;

    abstract public function runCustom(): void;

    /**
     * @throws Exception
     */
    public function __construct(){
        $this->clients = ClientModel::all();

        if(!$this->clients){
            throw new Exception('Before migrating tables to client databases, first seeding the client table');
        }
    }

    public function run(): void
    {
        $this->clients->each(function ($client) {
            Client::setConnection($client->id);

            $this->runCustom();
        });
    }
}
