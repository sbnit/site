<?php
namespace App\Interfaces\Repositories;

interface SettingsRepositories extends RepositoryInterface
{
    public function getByKey(string $key);
}
