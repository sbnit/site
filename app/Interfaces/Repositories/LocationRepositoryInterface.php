<?php
namespace App\Interfaces\Repositories;

use App\Models\Location as Model;

interface LocationRepositoryInterface extends RepositoryInterface
{
    public function getByName(string $name): Model;
}
