<?php

use App\Http\Middleware\Location;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Views'], function() {
    Route::get('/', 'HomeController@view');
});

Route::group(['namespace' => 'Actions'], function() {
    Route::get('location', 'LocationController@get');
    Route::post('location', 'LocationController@post');

    Route::get('language', 'LanguageController@get');
});

