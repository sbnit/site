# * Lara-Serve
FROM php:8.0-fpm-alpine AS lara-serve

# Set variable
ENV APP_ROOT '/srv'
ENV APP_USER 'docker'
ENV APP_GROUP 'docker'

# Set working directory
WORKDIR ${APP_ROOT}

# Install dependencies
RUN apk --update --no-cache add wget \
  curl \
  git \
  build-base \
  postgresql-dev \
  libzip-dev \
  libpng-dev \
  make \
  autoconf \
  zip \
  g++ 

# Install extensions
RUN docker-php-ext-install pdo \
  pdo_mysql \
  pdo_pgsql \
  zip \
  xml \
  exif \
  pcntl \
  gd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel application
RUN addgroup -g 1000 ${APP_GROUP} && adduser -HD -S ${APP_USER} -s /bin/bash -u 1000 -G ${APP_GROUP}

# Copy existing application directory contents
ADD . ${APP_ROOT}

# Copy existing application directory permissions
RUN chown -R ${APP_USER}:${APP_GROUP} ${APP_ROOT}

# Add full permissions directory storage
RUN chmod -R 775 ${APP_ROOT} && chmod -R 777 ${APP_ROOT}/storage

# Change current user
USER ${APP_USER}

# Laravel setting
RUN cd ${APP_ROOT}
#RUN composer update --lock --no-cache
RUN composer install --no-cache
RUN php artisan config:cache
RUN php artisan cache:clear

CMD php artisan serve --host=0.0.0.0 --port=8181
EXPOSE 8181

# * Development
FROM php:8.0-fpm-alpine AS dev

# Set variable
ENV APP_ROOT '/srv'
ENV APP_USER 'docker'
ENV APP_GROUP 'docker'

# Set working directory
WORKDIR ${APP_ROOT}

RUN echo 'Build dev contaiiner'

# Install dependencies servers
RUN apk --update --no-cache add wget \
  curl \
  git \
  build-base \
  postgresql-dev \
  libzip-dev \
  libpng-dev \
  libxml2-dev \
  make \
  autoconf \
  zip \
  g++ 

# Install extensions
RUN docker-php-ext-install pdo \
  pdo_pgsql \
  zip \
  xml \
  exif \
  pcntl \
  gd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel application
RUN addgroup -g 1000 ${APP_GROUP} && adduser -HD -S ${APP_USER} -s /bin/bash -u 1000 -G ${APP_GROUP}

# Copy existing application directory contents
ADD . ${APP_ROOT}

# Copy existing application directory permissions
RUN chown -R ${APP_USER}:${APP_GROUP} ${APP_ROOT}

# Add full permissions directory storage
RUN chmod -R 775 ${APP_ROOT} && chmod -R 777 ${APP_ROOT}/storage

# Change current user
USER ${APP_USER}

# Laravel setting
RUN cd ${APP_ROOT}
# RUN composer update --lock --no-cache
RUN composer install --no-cache
RUN php artisan optimize

EXPOSE 9000
CMD ["php-fpm"]

# * Production
FROM php:8.0-fpm-alpine AS prod

# Set variable
ENV APP_ROOT '/srv'
ENV APP_USER 'docker'
ENV APP_GROUP 'docker'

# Set working directory
WORKDIR ${APP_ROOT}

# Install dependencies
RUN apk --update --no-cache add wget \
  curl \
  git \
  build-base \
  postgresql-dev \
  libzip-dev \
  libpng-dev \
  make \
  autoconf \
  zip \
  g++ 

# Install extensions
RUN docker-php-ext-install pdo \
  pdo_mysql \
  pdo_pgsql \
  zip \
  xml \
  exif \
  pcntl \
  gd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel application
RUN addgroup -g 1000 ${APP_GROUP} && adduser -HD -S ${APP_USER} -s /bin/bash -u 1000 -G ${APP_GROUP}

# Copy existing application directory contents
ADD . ${APP_ROOT}

# Copy existing application directory permissions
RUN chown -R ${APP_USER}:${APP_GROUP} ${APP_ROOT}

# Add full permissions directory storage
RUN chmod -R 775 ${APP_ROOT} && chmod -R 777 ${APP_ROOT}/storage

# Change current user
USER ${APP_USER}

# Laravel setting
RUN cd ${APP_ROOT}
# RUN composer update --lock --no-cache
RUN composer install --no-cache
RUN php artisan config:cache
RUN php artisan cache:clear

EXPOSE 9000
CMD ["php-fpm"]

