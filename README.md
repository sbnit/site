<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Settings project for first start

**Короткая команда для настройки проекта**
```bash
$ docker-compose exec site php artisan laravel:setting
```
**Или**
```bash
$ docker-compose exec site php artisan key:generate
$ docker-compose exec site php artisan migrate
$ docker-compose exec site php artisan db:seed --class=ClientSeeder
$ docker-compose exec site php artisan slave:create
$ docker-compose exec site php artisan migrate --patch=database/migrations/clients
$ docker-compose exec site php artisan db:seed
```
