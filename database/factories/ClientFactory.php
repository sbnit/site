<?php
namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $country = explode(' ', $this->faker->country());
        $database = mb_strtolower(mb_strimwidth($country[0], 0, 2));
        $firstName = mb_strtolower($this->faker->firstName());

        return [
            'name' => mb_strtolower($country[0]),
            'host' => 'db',
            'username' => $firstName,
            'password' => 'password',
            'database' => 'sbnit_'.$database,
        ];
    }
}
