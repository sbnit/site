<?php
namespace Database\Factories\Clients;

use App\Models\Clients\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'phone' => $this->faker->e164PhoneNumber(),
            'email' => $this->faker->email()
        ];
    }
}
