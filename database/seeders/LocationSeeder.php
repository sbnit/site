<?php

namespace Database\Seeders;

use App\Models\Location;
use Exception;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    private const LOCATIONS = [
        [
            'name' => 'Russia',
            'client_id' => 1
        ],
        [
            'name' => 'USA',
            'client_id' => 2
        ],
        [
            'name' => 'Italy',
            'client_id' => 3
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run(): void
    {
        foreach ($this::LOCATIONS as $location) {
            Location::firstOrCreate($location);
        }
    }
}
