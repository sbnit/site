<?php
namespace Database\Seeders;

use Database\Seeders\Clients\SettingSeeder;
use Database\Seeders\Clients\SurveySeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        // Seeding main db
        $this->call(LanguageSeeder::class);
        $this->call(LocationSeeder::class);

        // DSeeding clients db
        $this->runClients();
    }

    private function runClients(): void
    {
        $this->call(SurveySeeder::class);
        $this->call(SettingSeeder::class);
    }
}
