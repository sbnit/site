<?php
namespace Database\Seeders;

use App\Models\Client;
use Exception;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    private const CLIENTS = [
        [
            'name' => 'russia',
            'host' => 'db',
            'username' => 'test',
            'password' => 'password',
            'database' => 'sbnit_ru'
        ],
        [
            'name' => 'usa',
            'host' => 'db',
            'username' => 'test',
            'password' => 'password',
            'database' => 'sbnit_us'
        ],
        [
            'name' => 'italy',
            'host' => 'db',
            'username' => 'test',
            'password' => 'password',
            'database' => 'sbnit_it'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run(): void
    {
        foreach ($this::CLIENTS as $client){
            Client::firstOrCreate($client);
        }
    }
}
