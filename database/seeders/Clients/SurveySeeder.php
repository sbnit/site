<?php
namespace Database\Seeders\Clients;

use App\Abstracts\Clients\SeederAbstract;
use App\Models\Clients\Survey;

class SurveySeeder extends SeederAbstract
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function runCustom(): void
    {
        Survey::factory()
            ->count(30)
            ->create();
    }
}
