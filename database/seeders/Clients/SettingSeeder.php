<?php
namespace Database\Seeders\Clients;

use App\Abstracts\Clients\SeederAbstract;
use App\Models\Clients\Setting;
use Faker\Factory;

class SettingSeeder extends SeederAbstract
{
    private const SETTINGS = [
        [
            'key' => 'phone',
            'value' => '+19999999999',
        ],
        [
            'key' => 'email',
            'value' => 'exmaple@domain.com',
        ]
    ];

    /**
     * Run the database custom seeds.
     *
     * @return void
     */
    public function runCustom(): void
    {
        foreach ($this::SETTINGS as $setting){
            Setting::firstOrCreate($setting);
        }

    }
}
