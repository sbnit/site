<?php

namespace Database\Seeders\Clients;

use App\Abstracts\Clients\SeederAbstract;
use App\Models\Clients\Contact;

class ContactSeeder extends SeederAbstract
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function runCustom(): void
    {
        Contact::factory()
            ->count(1)
            ->create();
    }
}
