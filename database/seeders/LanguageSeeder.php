<?php

namespace Database\Seeders;

use App\Models\Language;
use Exception;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    private const LANGUAGES = [
        [
            'name' => 'Russian',
            'shortname' => 'ru'
        ],
        [
            'name' => 'English',
            'shortname' => 'en'
        ],
        [
            'name' => 'Italian',
            'shortname' => 'it'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run(): void
    {
        foreach ($this::LANGUAGES as $language){
            Language::firstOrCreate($language);
        }
    }
}
