<?php

use App\Abstracts\Clients\MigrationAbstract;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveysTable extends MigrationAbstract
{
    /**
     * Run the migrations.
     *
     * @param string $connection
     * @return void
     */
    public function upCustom(string $connection): void
    {
        Schema::connection($connection)->create('surveys', function (Blueprint $table) {
            $table->id();
            $table->string('text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @param string $connection
     * @return void
     */
    public function downCustom(string $connection): void
    {
        Schema::connection($connection)->dropIfExists('surveys');
    }
}
