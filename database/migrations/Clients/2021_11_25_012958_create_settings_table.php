<?php

use App\Abstracts\Clients\MigrationAbstract;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends MigrationAbstract
{
    public function upCustom(string $connection): void
    {
        Schema::connection($connection)->create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('key', 128)->nullable(false);
            $table->string('value', 3000)->nullable(false);
            $table->timestamps();
        });
    }

    public function downCustom(string $connection): void
    {
        Schema::connection($connection)->dropIfExists('settings');
    }
}
