<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

if(!function_exists('getCurrentLocale')){
    function getCurrentLocale(): array
    {
        $locale = [];

        try {
            $ip = file_get_contents("http://ipecho.net/plain");
            $url = 'http://ip-api.com/json/' . $ip;
            $tz = file_get_contents($url);

            $locale = json_decode($tz, true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        return $locale;
    }
}

if(!function_exists('databaseDefault')){
    function databaseDefault(): string
    {
        return Config::get('database.default');
    }
}
