<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyChangeLogo': true, 'stickyStartAt': 120, 'stickyHeaderContainerHeight': 85}">
    <div class="header-body border-top-0">
        <div class="header-top header-top-default header-top-borders border-bottom-0 bg-color-light">
            <div class="container">
                <div class="header-row">
                    <div class="header-column justify-content-between">
                        <div class="header-row">
                            <nav class="header-nav-top w-100 w-md-50pct w-xl-100pct">
                                <ul class="nav nav-pills d-inline-flex custom-header-top-nav-background pe-5">
                                    <li class="nav-item py-2 d-inline-flex z-index-1">
                                        <span class="d-flex align-items-center p-0">
                                            <span>
                                                <img width="25" src="{{asset('img/demos/business-consulting-3/icons/phone.svg')}}" alt="Phone Icon" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-light'}" />
                                            </span>
                                            <a class="text-color-light text-decoration-none font-weight-semibold text-3-5 ms-2" href="tel:{{$data['phone']}}" data-cursor-effect-hover="plus" data-cursor-effect-hover-color="light">{{$data['phone']}}</a>
                                        </span>
                                        <span class="font-weight-normal align-items-center px-0 d-none d-xl-flex ms-3">
														<span>
															<img width="25" src="{{asset('img/demos/business-consulting-3/icons/email.svg')}}" alt="Email Icon" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-light'}" />
														</span>
														<a class="text-color-light text-decoration-none font-weight-semibold text-3-5 ms-2" href="mailto:{{$data['email']}}" data-cursor-effect-hover="plus" data-cursor-effect-hover-color="light">{{$data['email']}}</a>
													</span>
                                    </li>
                                </ul>
                            </nav>
                            <div class="d-flex align-items-center w-100">
                                <ul class="ps-0 ms-auto mb-0">
                                    <li class="nav-item font-weight-semibold text-1 text-lg-2 text-color-dark d-none d-md-flex justify-content-end me-3">
                                        {{__('header.worksTime')}}
                                    </li>
                                </ul>
                                <ul class="social-icons social-icons-clean social-icons-icon-dark social-icons-big m-0 ms-lg-2">
                                    <li class="social-icons-instagram">
                                        <a href="https://www.instagram.com/" target="_blank" class="text-4" title="Instagram" data-cursor-effect-hover="fit"><i class="fab fa-instagram"></i></a>
                                    </li>
                                    <li class="social-icons-twitter">
                                        <a href="https://www.twitter.com/" target="_blank" class="text-4" title="Twitter" data-cursor-effect-hover="fit"><i class="fab fa-twitter"></i></a>
                                    </li>
                                    <li class="social-icons-facebook">
                                        <a href="https://www.facebook.com/" target="_blank" class="text-4" title="Facebook" data-cursor-effect-hover="fit"><i class="fab fa-facebook-f"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-container container" style="height: 117px;">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="{{url('/')}}">
                                <img alt="Porto" width="162" height="33" src="{{asset('img/demos/business-consulting-3/logo.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end w-100">
                    <div class="header-row">
                        <div class="header-nav header-nav-links order-2 order-lg-1">
                            <div class="header-nav-main header-nav-main-square header-nav-main-text-capitalize header-nav-main-effect-1 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li>
                                            <a class="nav-link active" href="{{url('/')}}">
                                                {{__('header.home')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="{{url('/about')}}">
                                                {{__('header.about')}}
                                            </a>
                                        </li>
                                        <li class="dropdown">
                                            <a class="nav-link dropdown-toggle" href="javascript:void(0)">
                                                {{__('header.services.name')}}
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{url('services/web')}}" class="dropdown-item">{{__('header.services.child.web')}}</a>
                                                </li>
                                                <li>
                                                    <a href="{{url('services/mobile')}}" class="dropdown-item">{{__('header.services.child.mobile')}}</a>
                                                </li>
                                                <li>
                                                    <a href="{{url('services/desktop')}}" class="dropdown-item">{{__('header.services.child.desktop')}}</a>
                                                </li>
                                                <li>
                                                    <a href="{{url('services/support')}}" class="dropdown-item">{{__('header.services.child.support')}}</a>
                                                </li>
                                                <li>
                                                    <a href="{{url('services/seo')}}" class="dropdown-item">{{__('header.services.child.seo')}}</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="{{url('/services/cases')}}">
                                                {{__('header.cases')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="{{url('/services/blog')}}">
                                                {{__('header.blog')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="{{url('/services/contacts')}}">
                                                {{__('header.contacts')}}
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-column header-column-search justify-content-end align-items-center d-flex w-auto flex-row">
                    <a href="javascript:void(0)" class="btn btn-dark custom-btn-style-1 font-weight-semibold text-3-5 btn-px-3 py-2 ws-nowrap ms-4 d-none d-lg-block" data-cursor-effect-hover="plus" data-cursor-effect-hover-color="light"><span>{{__('header.order-dev')}}</span></a>

                </div>
            </div>
        </div>
    </div>
</header>
