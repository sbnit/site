@extends('container')

@section('content')
    <section class="section section-height-3 section-with-shape-divider position-relative border-0 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'parallaxHeight': '120%'}" data-image-src="img/demos/business-consulting-3/backgrounds/background-1.jpg">
        <div class="owl-carousel owl-carousel-light owl-carousel-light-init-fadeIn owl-theme manual dots-inside dots-horizontal-center show-dots-hover nav-inside nav-inside-plus nav-dark nav-md nav-font-size-md show-nav-hover mb-0" data-plugin-options="{'autoplayTimeout': 7000}" data-dynamic-height="['calc(100vh - 171px)','calc(100vh - 171px)','calc(100vh - 171px)','calc(100vh - 171px)','calc(100vh - 171px)']" style="height: calc(100vh - 171px)">
            <div class="owl-stage-outer">
                <div class="owl-stage">

                    <!-- Carousel Slide 1 -->
                    <div class="owl-item position-relative overlay overlay-show overlay-op-8 lazyload" data-bg-src="{{asset('img/home/home__slider/slider-1.jpg')}}" style="background-size: cover; background-position: center;">
                        <div class="container position-relative z-index-3 h-100">
                            <div class="row justify-content-center align-items-center h-100">
                                <div class="col-lg-6">
                                    <div class="d-flex flex-column align-items-center">
                                        <h3 class="position-relative text-color-light text-5 line-height-5 font-weight-medium px-4 mb-2 appear-animation" data-appear-animation="fadeInDownShorter" data-plugin-options="{'minWindowWidth': 0}">
													<span class="position-absolute right-100pct top-50pct transform3dy-n50 opacity-3">
														<img src="{{asset('img/lazy.png')}}" data-src="{{asset('img/slide-title-border.png')}}" class="w-auto appear-animation lazyload" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="250" data-plugin-options="{'imgFluid': false, 'minWindowWidth': 0}" alt="" />
													</span>
                                            {{__('home.sliders.fistSlide.smallText')}}
                                            <span class="position-absolute left-100pct top-50pct transform3dy-n50 opacity-3">
														<img src="{{asset('img/lazy.png')}}" data-src="{{asset('img/slide-title-border.png')}}" class="w-auto appear-animation lazyload" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="250" data-plugin-options="{'imgFluid': false, 'minWindowWidth': 0}" alt="" />
													</span>
                                        </h3>
                                        <h2 class="text-color-light font-weight-extra-bold text-12 mb-3 appear-animation" data-appear-animation="blurIn" data-appear-animation-delay="500" data-plugin-options="{'minWindowWidth': 0}">{{__('home.sliders.fistSlide.title')}}</h2>
                                        <p class="text-4 text-color-light font-weight-light opacity-7 text-center mb-0" data-plugin-animated-letters data-plugin-options="{'startDelay': 1000, 'minWindowWidth': 0, 'animationSpeed': 30}">{{__('home.sliders.fistSlide.span')}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Carousel Slide 2 -->
                    <div class="owl-item position-relative overlay overlay-show overlay-op-8 lazyload" data-bg-src="{{asset('img/home/home__slider/slider-2.jpg')}}" style="background-size: cover; background-position: center;">
                        <div class="container position-relative z-index-3 h-100">
                            <div class="row justify-content-center align-items-center h-100">
                                <div class="col-lg-6">
                                    <div class="d-flex flex-column align-items-center">
                                        <h3 class="position-relative text-color-light text-5 line-height-5 font-weight-medium px-4 mb-2 appear-animation" data-appear-animation="fadeInDownShorter" data-plugin-options="{'minWindowWidth': 0}">
													<span class="position-absolute right-100pct top-50pct transform3dy-n50 opacity-3">
														<img src="{{asset('img/lazy.png')}}" data-src="{{asset('img/slide-title-border.png')}}" class="w-auto appear-animation lazyload" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="250" data-plugin-options="{'imgFluid': false, 'minWindowWidth': 0}" alt="" />
													</span>
                                            {{__('home.sliders.secondSlide.smallText')}}
                                            <span class="position-absolute left-100pct top-50pct transform3dy-n50 opacity-3">
														<img src="{{asset('img/lazy.png')}}" data-src="{{asset('img/slide-title-border.png')}}" class="w-auto appear-animation lazyload" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="250" data-plugin-options="{'imgFluid': false, 'minWindowWidth': 0}" alt="" />
													</span>
                                        </h3>
                                        <h2 class="text-color-light font-weight-extra-bold text-12 mb-3 appear-animation" data-appear-animation="blurIn" data-appear-animation-delay="500" data-plugin-options="{'minWindowWidth': 0}">{{__('home.sliders.secondSlide.title')}}</h2>
                                        <p class="text-4 text-color-light font-weight-light opacity-7 text-center mb-0" data-plugin-animated-letters data-plugin-options="{'startDelay': 1000, 'minWindowWidth': 0, 'animationSpeed': 30}">{{__('home.sliders.secondSlide.span')}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Carousel Slide 3 -->
                    <div class="owl-item position-relative overlay overlay-color-primary overlay-show overlay-op-8 lazyload" data-bg-src="{{asset('img/home/home__slider/slider-3.jpg')}}" style="background-size: cover; background-position: center;">
                        <div class="container position-relative z-index-3 h-100">
                            <div class="row justify-content-center align-items-center h-100">
                                <div class="col-lg-6">
                                    <div class="d-flex flex-column align-items-center">
                                        <h3 class="position-relative text-color-light text-4 line-height-5 font-weight-medium px-4 mb-2 appear-animation" data-appear-animation="fadeInDownShorter" data-plugin-options="{'minWindowWidth': 0}">
													<span class="position-absolute right-100pct top-50pct transform3dy-n50 opacity-3">
														<img src="{{asset('img/lazy.png')}}" data-src="{{asset('img/slide-title-border.png')}}" class="w-auto appear-animation lazyload" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="250" data-plugin-options="{'imgFluid': false, 'minWindowWidth': 0}" alt="" />
													</span>
                                            Social Business Network Information Technologies
                                            <span class="position-absolute left-100pct top-50pct transform3dy-n50 opacity-3">
														<img src="{{asset('img/lazy.png')}}" data-src="{{asset('img/slide-title-border.png')}}" class="w-auto appear-animation lazyload" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="250" data-plugin-options="{'imgFluid': false, 'minWindowWidth': 0}" alt="" />
													</span>
                                        </h3>
                                        <h2 class="porto-big-title text-color-light font-weight-extra-bold mb-3" data-plugin-animated-letters data-plugin-options="{'startDelay': 1000, 'minWindowWidth': 0, 'animationSpeed': 300, 'animationName': 'fadeInRightShorterOpacity', 'letterClass': 'd-inline-block'}">SBNIT</h2>
                                        <p class="text-4 text-color-light font-weight-light text-center mb-0" data-plugin-animated-letters data-plugin-options="{'startDelay': 2000, 'minWindowWidth': 0}">Единство расширяет границы</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="owl-nav">
                <button type="button" role="presentation" class="owl-prev"></button>
                <button type="button" role="presentation" class="owl-next"></button>
            </div>
            <div class="owl-dots mb-5">
                <button role="button" class="owl-dot active"><span></span></button>
                <button role="button" class="owl-dot"><span></span></button>
                <button role="button" class="owl-dot"><span></span></button>
            </div>
        </div>
    </section>

    @if($data['survey'])
    <div class="home-intro bg-primary" id="home-intro">
        <div class="container">

            <div class="row align-items-center">
                <div class="col-lg-8">
                    <p>
                        <span class="highlighted-word text-3">{{$data['survey']}}</span>
                    </p>
                </div>
                <div class="col-lg-4">
                    <div class="get-started text-start text-lg-end">
                        <a href="#" class="btn btn-light btn-lg text-3 font-weight-semibold px-4 py-3">Get Started Now</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endif

    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-11 col-xl-10 text-center">
                <h2 class="custom-highlight-text-1 d-inline-block line-height-5 text-4 positive-ls-3 font-weight-medium text-color-primary mb-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250">ТОЛЬКО В ПЕРЕД</h2>
                <h3 class="text-9 line-height-3 text-transform-none font-weight-semibold mb-3 pb-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500">Процесс работы</h3>
                <p class="text-3-5 pb-3 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="750">Мы превратим ваши желания - в отличные возможности </p>
            </div>
        </div>

    </div>

    <div class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
        <div class="home-concept mt-5">
            <div class="container">

                <div class="row text-center">
                    <span class="sun"></span>
                    <span class="cloud"></span>
                    <div class="col-lg-2 ms-lg-auto">
                        <div class="process-image">
                            <img src="{{asset('img/home/home__process/services__strategy.png')}}" alt="" />
                            <strong>Strategy</strong>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="process-image process-image-on-middle">
                            <img src="{{asset('img/home/home__process/services__planning.png')}}" alt="" />
                            <strong>Planning</strong>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="process-image">
                            <img src="{{asset('img/home/home__process/services__build.png')}}" alt="" />
                            <strong>Build</strong>
                        </div>
                    </div>
                    <div class="col-lg-4 ms-lg-auto">
                        <div class="project-image">
                            <div id="fcSlideshow" class="fc-slideshow">
                                <ul class="fc-slides">
                                    <li><a href="javascript:void(0)"><img class="img-fluid" src="{{asset('img/home/home__process/works/services__works-1.jpg')}}" alt="" /></a></li>
                                    <li><a href="javascript:void(0)"><img class="img-fluid" src="{{asset('img/home/home__process/works/services__works-2.jpg')}}" alt="" /></a></li>
                                    <li><a href="javascript:void(0)"><img class="img-fluid" src="{{asset('img/home/home__process/works/services__works-3.jpg')}}" alt="" /></a></li>
                                </ul>
                            </div>
                            <strong class="our-work">Our Work</strong>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container" style="margin-top: 150px;">

        <div id="aboutus" class="row align-items-xl-center pt-4 mt-5">
            <div class="col-md-10 col-lg-6 mb-5 mb-lg-0">
                <div class="row row-gutter-sm">
                    <div class="col-6">
                        <img src="{{asset('img/home/home__about/about-1.png')}}" class="img-fluid box-shadow-5" alt="" />
                    </div>
                    <div class="col-6">
                        <img src="{{asset('img/home/home__about/about-2.png')}}" class="img-fluid box-shadow-5 mb-4" alt="" />
                        <img src="{{asset('img/home/home__about/about-3.png')}}" class="img-fluid box-shadow-5" alt="" />
                    </div>
                </div>
            </div>
            <div class="col-lg-6 ps-lg-4 ps-xl-5">
                <h2 class="custom-highlight-text-1 d-inline-block line-height-5 text-4 positive-ls-3 font-weight-medium text-color-primary mb-2 appear-animation" data-appear-animation="fadeInUpShorter">ABOUT US</h2>
                <h3 class="text-9 text-lg-5 text-xl-9 line-height-3 text-transform-none font-weight-semibold mb-4 mb-lg-3 mb-xl-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250">Мы предлагаем самые передовые стратегии для вашего бизнеса</h3>
                <p class="text-3-5 pb-1 mb-4 mb-lg-2 mb-xl-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500">Cras a elit sit amet leo accumsan volutpat. Suspendisse hendreriast ehicula leo, vel efficitur felis ultrices non. Cras a elit sit amet leo acun volutpat. Suspendisse hendrerit vehicula leo, vel efficitur fel. (короткий текст о нас)</p>
                <div class="row align-items-center pb-2 mb-4 mb-lg-1 mb-xl-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="750">
                    <div class="col-5">
                        <div class="d-flex">
                            <img width="63" height="63" src="img/demos/business-consulting-3/icons/label.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'd-lg-none d-xl-block'}" />
                            <span class="text-3 font-weight-bold text-color-dark pt-2 ms-2">
											<strong class="d-block font-weight-bold text-9 mb-2">240+</strong>
											Довольных клиентов
										</span>
                        </div>
                    </div>
                    <div class="col-7">
                        <blockquote class="custom-blockquote-style-1 m-0 pt-1 pb-2">
                            <p class="mb-0">Cras a elit sit amet leo accumsan volutpat. Suspendisse. </p>
                        </blockquote>
                    </div>
                </div>
                <div class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1000">
                    <a href="demo-business-consulting-3-contact.html" class="btn btn-primary custom-btn-style-1 font-weight-semibold btn-px-4 btn-py-2 text-3-5" data-cursor-effect-hover="plus" data-cursor-effect-hover-color="light"><span>Get a Quote</span></a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid pt-5 mt-5 mb-4">
        <div class="row">
            <div class="col appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250">
                <div class="owl-carousel owl-theme carousel-center-active-item custom-carousel-vertical-center-items custom-dots-style-1" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 3}, '768': {'items': 3}, '992': {'items': 5}, '1200': {'items': 5}, '1600': {'items': 7}}, 'autoplay': false, 'autoplayTimeout': 3000, 'dots': true}">
                    <div class="text-center">
                        <img class="d-inline-block img-fluid" src="img/logos/logo-8.png" alt="" style="max-width: 90px;" />
                    </div>
                    <div class="text-center">
                        <img class="d-inline-block img-fluid" src="img/logos/logo-9.png" alt="" style="max-width: 140px;" />
                    </div>
                    <div class="text-center">
                        <img class="d-inline-block img-fluid" src="img/logos/logo-10.png" alt="" style="max-width: 140px;" />
                    </div>
                    <div class="text-center">
                        <img class="d-inline-block img-fluid" src="img/logos/logo-11.png" alt="" style="max-width: 140px;" />
                    </div>
                    <div class="text-center">
                        <img class="d-inline-block img-fluid" src="img/logos/logo-12.png" alt="" style="max-width: 100px;" />
                    </div>
                    <div class="text-center">
                        <img class="d-inline-block img-fluid" src="img/logos/logo-13.png" alt="" style="max-width: 100px;" />
                    </div>
                    <div class="text-center">
                        <img class="d-inline-block img-fluid" src="img/logos/logo-14.png" alt="" style="max-width: 140px;" />
                    </div>
                    <div class="text-center">
                        <img class="d-inline-block img-fluid" src="img/logos/logo-15.png" alt="" style="max-width: 110px;" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="section section-height-4 section-with-shape-divider bg-color-grey-scale-1 border-0 pb-5 m-0">
        <div class="shape-divider" style="height: 123px;">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 123" preserveAspectRatio="xMinYMin">
                <polygon fill="#F3F3F3" points="0,90 221,60 563,88 931,35 1408,93 1920,41 1920,-1 0,-1 "/>
                <polygon fill="#FFFFFF" points="0,75 219,44 563,72 930,19 1408,77 1920,25 1920,-1 0,-1 "/>
            </svg>
        </div>
        <div class="container mt-4">
            <div class="row justify-content-center">
                <div class="col-lg-11 col-xl-10 text-center">
                    <h2 class="custom-highlight-text-1 d-inline-block line-height-5 text-4 positive-ls-3 font-weight-medium text-color-primary mb-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250">УСЛУГИ</h2>
                    <h3 class="text-9 line-height-3 text-transform-none font-weight-semibold mb-3 pb-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500">Наши услуги премиум-класса</h3>
                    <p class="text-3-5 pb-3 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="750">Cras a elit sit amet leo accumsan volutpat. Suspendisse hendreriast ehicula leo, vel efficitur felis ultrices non. Cras a elit sit amet leo acun volutpat. Suspendisse hendrerit vehicula leo, vel efficitur fel. </p>
                </div>
            </div>
            <div class="row row-gutter-sm justify-content-center mb-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1000">
                <div class="col-sm-9 col-md-6 col-lg-4 mb-4">
                    <a href="demo-business-consulting-3-services-detail.html" class="custom-link-hover-effects text-decoration-none" data-cursor-effect-hover="plus">
                        <div class="card box-shadow-4">
                            <div class="card-img-top position-relative overlay overlay-show">
                                <div class="position-absolute bottom-0 left-0 w-100 py-3 px-4 z-index-3">
                                    <h4 class="font-weight-semibold text-color-light text-6 mb-1">Разработка сайтов</h4>
                                    <div class="custom-crooked-line">
                                        <img width="154" height="26" src="img/demos/business-consulting-3/icons/infinite-crooked-line.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 154px;" />
                                    </div>
                                </div>
                                <img src="{{asset('img/home/home__services/service-site.png')}}" class="img-fluid" alt="Card Image" />
                            </div>
                            <div class="card-body d-flex align-items-center custom-view-more px-4">
                                <p class="card-text w-100 mb-0">Cras a elit sit amet leo accumsan. Suspendisse hendrerit. </p>
                                <img width="50" height="50" class="w-auto" src="img/demos/business-consulting-3/icons/arrow-right.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 50px;" />
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-9 col-md-6 col-lg-4 mb-4">
                    <a href="demo-business-consulting-3-services-detail.html" class="custom-link-hover-effects text-decoration-none" data-cursor-effect-hover="plus">
                        <div class="card box-shadow-4">
                            <div class="card-img-top position-relative overlay overlay-show">
                                <div class="position-absolute bottom-0 left-0 w-100 py-3 px-4 z-index-3">
                                    <h4 class="font-weight-semibold text-color-light text-6 mb-1">Разработка мобильных приложений</h4>
                                    <div class="custom-crooked-line">
                                        <img width="154" height="26" src="img/demos/business-consulting-3/icons/infinite-crooked-line.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 154px;" />
                                    </div>
                                </div>
                                <img src="{{asset('img/home/home__services/service-mobile.png')}}" class="img-fluid" alt="Card Image" />
                            </div>
                            <div class="card-body d-flex align-items-center custom-view-more px-4">
                                <p class="card-text w-100 mb-0">Cras a elit sit amet leo accumsan. Suspendisse hendrerit. </p>
                                <img width="50" height="50" class="w-auto" src="img/demos/business-consulting-3/icons/arrow-right.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 50px;" />
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-9 col-md-6 col-lg-4 mb-4">
                    <a href="demo-business-consulting-3-services-detail.html" class="custom-link-hover-effects text-decoration-none" data-cursor-effect-hover="plus">
                        <div class="card box-shadow-4">
                            <div class="card-img-top position-relative overlay overlay-show">
                                <div class="position-absolute bottom-0 left-0 w-100 py-3 px-4 z-index-3">
                                    <h4 class="font-weight-semibold text-color-light text-6 mb-1">Разработка desktop приложений</h4>
                                    <div class="custom-crooked-line">
                                        <img width="154" height="26" src="img/demos/business-consulting-3/icons/infinite-crooked-line.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 154px;" />
                                    </div>
                                </div>
                                <img src="{{asset('img/home/home__services/service-desktop.png')}}" class="img-fluid" alt="Card Image" />
                            </div>
                            <div class="card-body d-flex align-items-center custom-view-more px-4">
                                <p class="card-text w-100 mb-0">Cras a elit sit amet leo accumsan. Suspendisse hendrerit. </p>
                                <img width="50" height="50" class="w-auto" src="img/demos/business-consulting-3/icons/arrow-right.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 50px;" />
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-9 col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <a href="demo-business-consulting-3-services-detail.html" class="custom-link-hover-effects text-decoration-none" data-cursor-effect-hover="plus">
                        <div class="card box-shadow-4">
                            <div class="card-img-top position-relative overlay overlay-show">
                                <div class="position-absolute bottom-0 left-0 w-100 py-3 px-4 z-index-3">
                                    <h4 class="font-weight-semibold text-color-light text-6 mb-1">Cost Transformation</h4>
                                    <div class="custom-crooked-line">
                                        <img width="154" height="26" src="img/demos/business-consulting-3/icons/infinite-crooked-line.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 154px;" />
                                    </div>
                                </div>
                                <img src="img/demos/business-consulting-3/services/services-4.jpg" class="img-fluid" alt="Card Image" />
                            </div>
                            <div class="card-body d-flex align-items-center custom-view-more px-4">
                                <p class="card-text w-100 mb-0">Cras a elit sit amet leo accumsan. Suspendisse hendrerit. </p>
                                <img width="50" height="50" class="w-auto" src="img/demos/business-consulting-3/icons/arrow-right.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 50px;" />
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-9 col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <a href="demo-business-consulting-3-services-detail.html" class="custom-link-hover-effects text-decoration-none" data-cursor-effect-hover="plus">
                        <div class="card box-shadow-4">
                            <div class="card-img-top position-relative overlay overlay-show">
                                <div class="position-absolute bottom-0 left-0 w-100 py-3 px-4 z-index-3">
                                    <h4 class="font-weight-semibold text-color-light text-6 mb-1">Digital Marketing</h4>
                                    <div class="custom-crooked-line">
                                        <img width="154" height="26" src="img/demos/business-consulting-3/icons/infinite-crooked-line.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 154px;" />
                                    </div>
                                </div>
                                <img src="img/demos/business-consulting-3/services/services-5.jpg" class="img-fluid" alt="Card Image" />
                            </div>
                            <div class="card-body d-flex align-items-center custom-view-more px-4">
                                <p class="card-text w-100 mb-0">Cras a elit sit amet leo accumsan. Suspendisse hendrerit. </p>
                                <img width="50" height="50" class="w-auto" src="img/demos/business-consulting-3/icons/arrow-right.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 50px;" />
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-9 col-md-6 col-lg-4">
                    <a href="demo-business-consulting-3-services-detail.html" class="custom-link-hover-effects text-decoration-none" data-cursor-effect-hover="plus">
                        <div class="card box-shadow-4">
                            <div class="card-img-top position-relative overlay overlay-show">
                                <div class="position-absolute bottom-0 left-0 w-100 py-3 px-4 z-index-3">
                                    <h4 class="font-weight-semibold text-color-light text-6 mb-1">Automation</h4>
                                    <div class="custom-crooked-line">
                                        <img width="154" height="26" src="img/demos/business-consulting-3/icons/infinite-crooked-line.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 154px;" />
                                    </div>
                                </div>
                                <img src="img/demos/business-consulting-3/services/services-6.jpg" class="img-fluid" alt="Card Image" />
                            </div>
                            <div class="card-body d-flex align-items-center custom-view-more px-4">
                                <p class="card-text w-100 mb-0">Cras a elit sit amet leo accumsan. Suspendisse hendrerit. </p>
                                <img width="50" height="50" class="w-auto" src="img/demos/business-consulting-3/icons/arrow-right.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary'}" style="width: 50px;" />
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <a href="demo-business-consulting-3-services.html" class="btn btn-primary custom-btn-style-1 font-weight-semibold btn-px-4 btn-py-2 text-3-5" data-cursor-effect-hover="plus" data-cursor-effect-hover-color="light">
                        <span>All Services</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-height-4 section-with-shape-divider position-relative bg-dark border-0 m-0">
        <div class="shape-divider shape-divider-reverse-x z-index-3" style="height: 102px;">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 102" preserveAspectRatio="xMinYMin">
                <polygon fill="#F3F3F3" points="0,65 220,35 562,63 931,10 1410,68 1920,16 1920,103 0,103 "/>
                <polygon fill="#EDEDED" points="0,82 219,51 562,78 931,26 1409,83 1920,32 1920,103 0,103 "/>
            </svg>
        </div>
        <div class="position-absolute top-0 left-0 h-100 d-none d-lg-block overlay overlay-show overlay-color-primary" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="img/demos/business-consulting-3/parallax/parallax-1.jpg" style="width: 40%;"></div>
        <div class="container position-relative z-index-3 pt-5 mt-5">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <h2 class="custom-text-background custom-big-font-size-1 text-15 font-weight-bold float-end clearfix line-height-1 lazyload pe-xl-5 me-3 mb-0 d-none d-lg-block" data-bg-src="img/demos/business-consulting-3/backgrounds/text-background-2.jpg" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.6, 'transition': true, 'horizontal': true, 'transitionDuration': 1000, 'isInsideSVG': true}">BENEFITS</h2>
                </div>
                <div class="col-lg-6">
                    <h2 class="custom-highlight-text-1 d-inline-block line-height-5 text-4 positive-ls-3 font-weight-medium text-color-primary mb-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250">WHY US</h2>
                    <h3 class="text-9 line-height-3 text-transform-none font-weight-medium text-color-light ls-0 mb-3 pb-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500">Learn Here The Main Reasons Why You Should Choose Us</h3>
                    <p class="text-3-5 pb-2 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="750">Cras a elit sit amet leo accumsan volutpat. Suspendisse hendreriast ehicula leo, vel efficitur felis ultrices non. </p>
                    <ul class="list ps-0 pe-lg-5 mb-0">
                        <li class="d-flex align-items-start pb-1 mb-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1000">
                            <i class="fas fa-check text-color-light text-4 custom-bg-color-grey-1 rounded-circle p-3"></i>
                            <span class="text-3-5 ps-3">We wil identify where you're getting off-track in all areas and not just</span>
                        </li>
                        <li class="d-flex align-items-start pb-1 mb-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1250">
                            <i class="fas fa-check text-color-light text-4 custom-bg-color-grey-1 rounded-circle p-3"></i>
                            <span class="text-3-5 ps-3">We create a program that accelerates your strategic execution and growth goals.
										</span>
                        </li>
                        <li class="d-flex align-items-start appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1500">
                            <i class="fas fa-check text-color-light text-4 custom-bg-color-grey-1 rounded-circle p-3"></i>
                            <span class="text-3-5 ps-3">Consistently hit strategic objectives and revenue targets, and grow, year over year.</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-5 my-4">
        <div class="row justify-content-center pt-3">
            <div class="col-lg-10 text-center">
                <h2 class="custom-highlight-text-1 d-inline-block line-height-5 text-4 positive-ls-3 font-weight-medium text-color-primary mb-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250">THE BLOG</h2>
                <h3 class="text-9 line-height-3 text-transform-none font-weight-semibold mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500">Our Recent News</h3>
                <p class="text-3-5 pb-3 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="750">Cras a elit sit amet leo accumsan volutpat. Suspendisse hendreriast ehicula leo, vel efficitur felis ultrices non. Cras a elit sit amet leo acun volutpat. Suspendisse hendrerit vehicula leo, vel efficitur fel. </p>
            </div>
        </div>
        <div class="row row-gutter-sm justify-content-center mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1000">
            <div class="col-sm-9 col-md-6 col-lg-4 mb-4 mb-lg-0">
                <a href="demo-business-consulting-3-blog-post.html" class="custom-link-hover-effects text-decoration-none" data-cursor-effect-hover="plus">
                    <div class="card border-0 box-shadow-4">
                        <div class="card-img-top position-relative overlay">
                            <div class="position-absolute bottom-10 right-0 d-flex justify-content-end w-100 py-3 px-4 z-index-3">
											<span class="custom-date-style-1 text-center bg-primary text-color-light font-weight-semibold text-5-5 line-height-2 px-3 py-2">
												<span class="position-relative z-index-2">
													18
													<span class="d-block custom-font-size-1 positive-ls-2 px-1">FEB</span>
												</span>
											</span>
                            </div>
                            <img src="img/demos/business-consulting-3/blog/blog-1.jpg" class="img-fluid" alt="Lorem Ipsum Dolor" />
                        </div>
                        <div class="card-body p-4">
                            <span class="d-block text-color-grey font-weight-semibold positive-ls-2 text-2">FINANCE</span>
                            <h4 class="font-weight-semibold text-5 text-color-hover-primary mb-2">Lorem ipsum dolor sit a met, consectetur</h4>
                            <span class="custom-view-more d-inline-flex font-weight-medium text-color-primary">
											View More
											<img width="27" height="27" src="img/demos/business-consulting-3/icons/arrow-right.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary ms-2'}" style="width: 27px;" />
										</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-9 col-md-6 col-lg-4 mb-4 mb-lg-0">
                <a href="demo-business-consulting-3-blog-post.html" class="custom-link-hover-effects text-decoration-none" data-cursor-effect-hover="plus">
                    <div class="card border-0 box-shadow-4">
                        <div class="card-img-top position-relative overlay">
                            <div class="position-absolute bottom-10 right-0 d-flex justify-content-end w-100 py-3 px-4 z-index-3">
											<span class="custom-date-style-1 text-center bg-primary text-color-light font-weight-semibold text-5-5 line-height-2 px-3 py-2">
												<span class="position-relative z-index-2">
													15
													<span class="d-block custom-font-size-1 positive-ls-2 px-1">FEB</span>
												</span>
											</span>
                            </div>
                            <img src="img/demos/business-consulting-3/blog/blog-2.jpg" class="img-fluid" alt="Lorem Ipsum Dolor" />
                        </div>
                        <div class="card-body p-4">
                            <span class="d-block text-color-grey font-weight-semibold positive-ls-2 text-2">FINANCE</span>
                            <h4 class="font-weight-semibold text-5 text-color-hover-primary mb-2">Lorem ipsum dolor sit a met, consectetur</h4>
                            <span class="custom-view-more d-inline-flex font-weight-medium text-color-primary">
											View More
											<img width="27" height="27" src="img/demos/business-consulting-3/icons/arrow-right.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary ms-2'}" style="width: 27px;" />
										</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-9 col-md-6 col-lg-4">
                <a href="demo-business-consulting-3-blog-post.html" class="custom-link-hover-effects text-decoration-none" data-cursor-effect-hover="plus">
                    <div class="card border-0 box-shadow-4">
                        <div class="card-img-top position-relative overlay">
                            <div class="position-absolute bottom-10 right-0 d-flex justify-content-end w-100 py-3 px-4 z-index-3">
											<span class="custom-date-style-1 text-center bg-primary text-color-light font-weight-semibold text-5-5 line-height-2 px-3 py-2">
												<span class="position-relative z-index-2">
													12
													<span class="d-block custom-font-size-1 positive-ls-2 px-1">FEB</span>
												</span>
											</span>
                            </div>
                            <img src="img/demos/business-consulting-3/blog/blog-3.jpg" class="img-fluid" alt="Lorem Ipsum Dolor" />
                        </div>
                        <div class="card-body p-4">
                            <span class="d-block text-color-grey font-weight-semibold positive-ls-2 text-2">FINANCE</span>
                            <h4 class="font-weight-semibold text-5 text-color-hover-primary mb-2">Lorem ipsum dolor sit a met, consectetur</h4>
                            <span class="custom-view-more d-inline-flex font-weight-medium text-color-primary">
											View More
											<img width="27" height="27" src="img/demos/business-consulting-3/icons/arrow-right.svg" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-primary ms-2'}" style="width: 27px;" />
										</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection
