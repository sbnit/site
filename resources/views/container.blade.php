<!DOCTYPE html>
<html class="light" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{$title}}</title>

    <meta name="keywords" content="{{$keywords}}" />
    <meta name="description" content="{{$description}}">
    <meta name="author" content="{{$author}}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Web Fonts  -->
    <link id="googleFonts" href="https://fonts.googleapis.com/css?family=family=Lora:400,400i,700,700i|Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/animate/animate.compat.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/owl.carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/magnific-popup/magnific-popup.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/select2/select2.min.css')}}">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('vendor/template/css/theme.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/template/css/theme-elements.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/template/css/theme-blog.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/template/css/theme-shop.css')}}">

    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{asset('vendor/circle-flip-slideshow/css/component.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/template/css/business-consulting-3.css')}}">

    <!-- Skin CSS -->
    <link id="skinCSS" rel="stylesheet" href="{{asset('vendor/template/css/skin-business-consulting-3.css')}}">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{mix('css/app.css')}}">

    <!-- Head Libs -->
    <script src="{{asset('vendor/modernizr/modernizr.min.js')}}"></script>

</head>
<body>

<div data-plugin-cursor-effect>
    <div class="body">
        @include('layouts.header')
        <div role="main" class="main">
            @yield('content')
        </div>
        @include('layouts.footer')
    </div>
</div>

<div class="modal fade" id="localeModal" aria-labelledby="localeModalLabel" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="tab-pane tab-pane-navigation active" id="formsStyleWithIcons">
                    <h4 class="mb-3">{{__('locale.fillDate')}}</h4>
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <form id="saveLocation" class="contact-form form-with-icons" novalidate="novalidate">
                                        <div class="contact-form-success alert alert-success d-none mt-4">
                                            <strong>{{__('global.success')}}!</strong>
                                            <div class="message"></div>
                                        </div>

                                        <div class="contact-form-error alert alert-danger d-none mt-4">
                                            <strong>{{__('global.error')}}!</strong>
                                            <div class="message"></div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col">
                                                <label class="form-label">{{__('locale.country')}}</label>
                                                <div>
                                                    <select class="select2-locale" name="locations" data-placeholder="{{__('locale.selectCountry')}}">
                                                        <option></option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col">
                                                <label class="form-label">{{__('locale.language')}}</label>
                                                <div class="position-relative">
                                                    <select class="select2-locale" name="languages" data-placeholder="{{__('locale.selectLanguage')}}">
                                                        <option></option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <button type="button" class="btn btn-outline btn-primary mb-2 w-100" data-type="send">
                                                    {{__('global.apply')}}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Vendor -->
<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/jquery.appear/jquery.appear.min.js')}}"></script>
<script src="{{asset('vendor/jquery.easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('vendor/jquery.cookie/jquery.cookie.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendor/jquery.validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
<script src="{{asset('vendor/lazysizes/lazysizes.min.js')}}"></script>
<script src="{{asset('vendor/isotope/jquery.isotope.min.js')}}"></script>
<script src="{{asset('vendor/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('vendor/vide/jquery.vide.min.js')}}"></script>
<script src="{{asset('vendor/vivus/vivus.min.js')}}"></script>

<!-- Theme Base, Components and Settings -->
<script src="{{asset('vendor/template/js/theme.js')}}"></script>

<!-- Double Vendor -->
<script src="{{asset('vendor/circle-flip-slideshow/js/jquery.flipshow.min.js')}}"></script>

<!-- Template -->
<script src="{{asset('vendor/template/js/view.home.js')}}"></script>

<!-- Theme Custom -->
<script src="{{mix('js/app.js')}}"></script>

<!-- Theme Initialization Files -->
<script src="{{asset('vendor/template/js/theme.init.js')}}"></script>

@if(!session('location') || !session('language'))
<script type="text/javascript">
    $(document).ready(function() {
        let $modal = $('#localeModal');

        $modal.modal({backdrop: 'static', keyboard: false}).modal('show');

        $('.select2-locale[name="locations"]').select2({
            dropdownParent: $modal,
            width: '100%',
            ajax: {
                url: 'location',
                method: 'get',
                dataType: 'json',
                data: function (params) {
                    let search = (!params.term || params.term === 'undefined') ? '' : params.term;
                    return { search }
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                }
            },
            'language': {
                'noResults': function(){
                    return "{{__('global.noResults')}}";
                },
                'searching': function (){
                    return "{{__('global.searching')}}";
                }
            },
        });

        $('.select2-locale[name="languages"]').select2({
            dropdownParent: $modal,
            width: '100%',
            ajax: {
                url: 'language',
                method: 'get',
                dataType: 'json',
                data: function (params) {
                    let search = (!params.term || params.term === 'undefined') ? '' : params.term;

                    return { search }
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                }
            },
            'language': {
                'noResults': function(){
                    return "{{__('global.noResults')}}";
                },
                'searching': function (){
                    return "{{__('global.searching')}}";
                }
            },
        });
    });
</script>
@endif
</body>
</html>
