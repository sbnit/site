<?php

return [
    'sliders' => [
        'fistSlide' => [
            'smallText' => 'Будьте среди первых',
            'title' => 'Франшиза',
            'span' => 'Первые 2 месяца услуги франчайзинга бесплатно'
        ],
        'secondSlide' => [
            'smallText' => 'Стань частью большой',
            'title' => 'Команды',
            'span' => 'Мы рады сотрудничеству с каждым! Присоединяйся к нашей команде'
        ]
    ],
    'services' => [
        
    ]
];
