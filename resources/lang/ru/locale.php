<?php

return [
    'fillDate' => 'Заполните данные',
    'country' => 'Страна',
    'language' => 'Язык',
    'selectCountry' => 'Выберете страну',
    'selectLanguage' => 'Выберете язык',
    'errorLocation' => 'Вы не выбрали страну',
    'errorLanguage' => 'Вы не выбрали язык',
    'noSelectCountry' => 'Вы не выбрали страну',
    'noSelectLanguage' => 'Вы не выбрали язык'
];
