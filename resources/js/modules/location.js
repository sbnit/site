import alert from '../libs/alert'

$('#saveLocation').on('click', '[data-type="send"]', function (e){
    e.preventDefault();

    const button = $(this);

    let location_id = $(e.delegateTarget).find('select[name="locations"]').val();
    let language_id = $(e.delegateTarget).find('select[name="languages"]').val();

    alert.init($(e.delegateTarget).find('.alert-danger'))

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: 'location',
        method: 'POST',
        dataType: 'json',
        data: {
            location_id,
            language_id
        },
        beforeSend: function () {
            button.attr('disabled', true)
            alert.hide()
        },
        success: function(data){
            location.reload();
        },
        error: function(xhr){
            let errors = xhr.responseJSON.errors

            alert.fade(errors)

            button.attr('disabled', false)
        }
    });
})
