const alert = {
    alert: null,

    init: (alert) => {
        this.alert = alert
    },

    fade: (errors) => {
        for(let error in errors){
            const thisError = errors[error]

            thisError.forEach((err) => {
                this.alert.find('.message').append(`<span class="alert-text">${err}</span>`);
            });
        }

        this.alert.removeClass('d-none');
    },

    hide: () => {
        this.alert.addClass('d-none');
        this.alert.find('.message').html('')
    }
}

module.exports = alert
